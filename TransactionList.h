//Pascale Vacher - February 18
//OOP Assignment Task 1c - Semester 2
//Group Number: 4
//Team: Laura Booth-Nias	b5008289			Computer Science for Games
//		Freya Jones			b6021099			Computer Science for Games
//		Joshua Edwards		b5019371			Computer Science for Games
//		Johnathan Garland	b6013546			Computer Science for Games

#ifndef TransactionListH
#define TransactionListH

//---------------------------------------------------------------------------
//TransactionList: class declaration
//---------------------------------------------------------------------------

#include "ListT.h"
#include "Transaction.h"

#include <cassert> 	// for assert()
#include <sstream>

class TransactionList {
public:
	void addNewTransaction(const Transaction&);
    const Transaction newestTransaction() const;
    const TransactionList olderTransactions() const;
	TransactionList getTransactionsUpToDate(const Date date_);
	const TransactionList getAllDepositTransactions() const;
	const TransactionList getMostRecentTransactions(int x) const;
	const TransactionList getTransactionsForAmount(double) const;
	const TransactionList getTransactionsForTitle (string) const;
	const TransactionList getTransactionsForDate(Date) const;
	const double getTotalTransactions() const;
    void deleteFirstTransaction();
    void deleteGivenTransaction(const Transaction&);
	void deleteTransactionsUpToDate(const Date date_, TransactionList temp_);
	int size() const;

	const string toFormattedString() const;		//return transactionlist as a (formatted) string
	ostream& putDataInStream(ostream& os) const;	//send TransactionList info into an output stream
	istream& getDataFromStream(istream& is);	//receive TransactionList info from an input stream

private:
    List<Transaction> listOfTransactions_;	//list of transactions
};

//---------------------------------------------------------------------------
//non-member operator functions
//---------------------------------------------------------------------------

ostream& operator<<(ostream&, const TransactionList&);	//insertion operator
istream& operator>>(istream& is, TransactionList& trl); //extraction operator

#endif

