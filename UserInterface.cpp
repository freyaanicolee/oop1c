//Pascale Vacher - February 18
//OOP Assignment Task 1c - Semester 2
//Group Number: 4
//Team: Laura Booth-Nias	b5008289			Computer Science for Games
//		Freya Jones			b6021099			Computer Science for Games
//		Joshua Edwards		b5019371			Computer Science for Games
//		Johnathan Garland	b6013546			Computer Science for Games


#include "UserInterface.h" 

//---------------------------------------------------------------------------
//public member functions
//---------------------------------------------------------------------------
UserInterface* UserInterface::m_pInstance = NULL;

UserInterface* UserInterface::getInstance() //return the instance or instantiate if not done so already.
{										    //can only access the instance with getInstance().
	if (!m_pInstance)
	{
		m_pInstance = new UserInterface;
	}
	return m_pInstance;
}

void UserInterface::wait() const
{
	cout << "\n";
	outputLine("Press RETURN to go back to menu...");
	char ch;
	cin.get(ch);
	cin.get(ch);
}
void UserInterface::endProgram() const
{
	cout << "\n";
	outputLine("Press RETURN to end program...");
	char ch;
	cin.get(ch);
	cin.get(ch);
}
int UserInterface::showMainMenuAndGetCommand() const
{
	system("cls");
	cout << "\n\n";
	outputHeader("WELCOME TO THE ATM");
	outputLine(" 0                            Leave ATM ");
	outputLine(" 1                      Enter your card ");
	outputLine("----------------------------------------");
	return (readInCommand());
}

int UserInterface::showAccountMenuAndGetCommand(const string& accNum) const
{
	outputHeader("ACCOUNT " + accNum + " MENU");
	outputLine(" 0                    Back to card menu ");
	outputLine(" 1                      Display balance ");
	outputLine(" 2                Withdraw from account ");
	outputLine(" 3                 Deposit into account ");
	outputLine(" 4                       Show statement ");
	outputLine(" 5                    Show all deposits ");
	outputLine(" 6                  Show mini statement ");
	outputLine(" 7                  Search Transactions ");
	outputLine(" 8    Clear all transactions up to date ");
	outputLine(" 9          Transfer to another account ");
	outputLine("----------------------------------------");
	return (readInCommand());
}

const string UserInterface::readInCardToBeProcessed() const {
	return askForInput("ENTER YOUR CARD NUMBER");
}

void UserInterface::showValidateCardOnScreen(int validCode, const string& cardNumber) const
{
	switch (validCode)
	{
	case VALID_CARD: {
		// Card exists and is accessible (and is not already open: TO BE IMPLEMENTED)
	} break;
	case UNKNOWN_CARD: {
		outputLine("ERROR: CARD " + cardNumber + " DOES NOT EXIST");
	} break;
	case EMPTY_CARD: {
		outputLine("ERROR: CARD " + cardNumber + " DOES NOT LINK TO ANY ACCOUNTS");
	} break;
	}
}

int UserInterface::showCardMenuAndGetCommand(const string& cardNumber) const
{
	outputHeader("CARD " + cardNumber + " MENU");
	outputLine(" 0           Stop banking & remove card ");
	outputLine(" 1            Manage individual account ");
	outputLine(" 2           Show total funds available ");
	outputLine("----------------------------------------");
	return (readInCommand());
}

void UserInterface::showCardAccounts(const string& cardNumber, const string& cardDetails) const
{
	outputHeader("CARD " + cardNumber + " ACCOUNTS");
	cout << cardDetails;
	outputLine("----------------------------------------\n");
}

void UserInterface::showSearchMenu() const
{
	outputHeader("SEARCH MENU");
	outputLine(" 0    Stop searching and return to menu");
	outputLine(" 1          Show transaction for amount ");
	outputLine(" 2           Show transaction for title ");
	outputLine(" 3            Show transaction for date ");
	outputLine("----------------------------------------");
}

const string UserInterface::readInAccountToBeProcessed() const {
	return askForInput("SELECT ACCOUNT TO MANAGE");
}
void UserInterface::showValidateAccountOnScreen(int validCode, const string& accNum) const
{
	switch (validCode)
	{
	case VALID_ACCOUNT:
	{
		// Account exists and is accessible with that card (and not already open: TO BE IMPLEMENTED)
	} break;
	case INVALID_ACCOUNT_TYPE:
	{
		outputLine("ERROR: ACCOUNT " + accNum + " IS NOT A RECOGNISED TYPE OF ACCOUNT!");
	} break;
	case UNKNOWN_ACCOUNT:
	{
		outputLine("ERROR: ACCOUNT " + accNum + " DOES NOT EXIST!");
	} break;
	case UNACCESSIBLE_ACCOUNT:
	{
		outputLine("ERROR: ACCOUNT " + accNum + " IS NOT ACCESSIBLE WITH THIS CARD!");
	} break;
	case ACTIVE_ACCOUNT:
	{
		outputLine("ERROR: ACCOUNT " + accNum + " IS ALREADY OPEN!");
	} break;
	}
}
void UserInterface::showMatchingTransactionsOnScreenDouble(double a,int n, string str) const
{
	if (n == 0)
	{
		cout << "	NO TRANSACTION IN BANK ACCOUNT MATCH THE SEARCH CRITERION GIVEN";
	}
	else
	{
		cout << "\n		ALL MATCHING TRANSACTIONS FOR AMOUNT \234" << a << " REQUESTED AT " << time.currentTime() << " ON " << date.currentDate();
		cout << str;
		cout << "\n		TOTAL TRANSACTIONS " << n;
	}
}
void UserInterface::showMatchingTransactionsOnScreenString(string a, int n, string str) const
{
	if (n == 0)
	{
		cout << "	NO TRANSACTION IN BANK ACCOUNT MATCH THE SEARCH CRITERION GIVEN";
	}
	else
	{
		cout << "\n		ALL MATCHING TRANSACTIONS FOR TITLE " << a << " REQUESTED AT " << time.currentTime() << " ON " << date.currentDate();
		cout << str;
		cout << "\n		TOTAL TRANSACTIONS " << n;
	}
}
void UserInterface::showMatchingTransactionsOnScreenDate(Date a, int n, string str) const
{
	if (n == 0)
	{
		cout << "		NO TRANSACTION IN BANK ACCOUNT MATCH THE SEARCH CRITERION GIVEN";
	}
	else
	{
		cout << "\n		ALL MATCHING TRANSACTIONS FOR DATE " << a << " REQUESTED AT " << time.currentTime() << " ON " << date.currentDate();
		cout << str;
		cout << "\n		TOTAL TRANSACTIONS " << n;
	}
}
void UserInterface::showAllDepositsOnScreen(bool noTransactions, string str, double totalTransations) const
{
	cout << "ALL DEPOSIT TRANSACTIONS REQUESTED AT " << time.currentTime() << " ON " << date.currentDate();
	cout << str;
}
void UserInterface::showTransactionsUpToDateOnScreen(bool isEmpty, double total, const string str) const 
{
	cout << "DELETION REQUEST RECIEVED AT " << time.currentTime() << " ON " << date.currentDate();
	cout << str << "\n";
}
void UserInterface::showMiniStatementOnScreen(bool isEmpty, double total, string str) const {
	if (!isEmpty)
	{
		cout << "RECENT TRANSACTIONS REQUESTED AT " << time.currentTime() << " ON " << date.currentDate();
		cout << str;
	}
	else
	{
		outputLine("NO TRANSACTIONS IN BANK ACCOUNT!");
	}
}
void UserInterface::showFundsAvailableOnScreen(bool isEmpty, string str, double total) const {
	if (!isEmpty)
	{
		cout << "      AVAILABLE FUNDS REQUESTED AT " << time.currentTime() << " ON " << date.currentDate();
		cout << str;
		cout << "\n      TOTAL: \234" << total;
	}
	else 
	{
		outputLine("NO ACCOUNT ACCESSIBLE WITH THIS CARD!");
	}
	
}
//static 
const string UserInterface::cardFilename(const string& cn) {
	//read in card name & produce cashcard filename
	return FILEPATH + "card_" + cn + ".txt";	//read in card name & produce cashcard filename
}
//static
const string UserInterface::accountFilename(const string& an) {
	return FILEPATH + "account_" + an + ".txt";
}
//input functions
Date UserInterface::readInValidDate(const Date cDate) const {
	cout << "Enter Date (XX XX XXXX): ";
	int d, m, y;
	cin >> d >> m >> y;
	Date date_(d, m, y);
	while (!date_.isValid(cDate))
	{
		cout << "Invalid date. Please enter again";
		readInValidDate(cDate);
	}
	return date_;
	//if (date_.isValid(cDate))
	//	return date_;
	//else {
	//	cout << "Invalid date. Please enter again";
	//	readInValidDate(cDate);
	//}
}
bool UserInterface::readInConfirmDeletion() const {
	cout << "Delete transactions? \n";
	cout << "Enter y to delete \n";
	string input;
	cin >> input;
	if (input == "y" || input == "Y")
		return true;
	else
		return false;
}
double UserInterface::readInWithdrawalAmount() const {
	//ask for the amount to withdraw
	outputLine("AMOUNT TO WITHDRAW: \234");
	return (readInPositiveAmount());
}
double UserInterface::readInDepositAmount() const {
	//ask for the amount to deposit
	outputLine("AMOUNT TO DEPOSIT: \234");
	return (readInPositiveAmount());
}
int UserInterface::readInNumberOfTransactions() const {
	outputLine("NUMBER OF TRANSACTIONS: ");
	return (readInPositiveAmount());
}
int UserInterface::readInSearchCommand() const{
	return (readInCommand());
}
int UserInterface::readInAmount() const {
	outputLine("ENTER AMOUNT TO SEARCH FOR: ");
	double amount;
	cin >> amount;
	return amount;
}
string UserInterface::readInTitle() const {
	string title;
	outputLine("ENTER TITLE TO SEARCH FOR: ");
	cin >> title;
	while (title.length() == 0) {
		outputLine("INVALID (NO TITLE GIVEN) - ENTER TITLE TO SEARCH FOR: ");
		cin >> title;
	}
	return title;
}
string UserInterface::readInAccountToRecieveFunds() const {
	return askForInput("SELECT ACCOUNT TO RECIEVE FUNDS: ");
}
double UserInterface::readInTransferAmount() const {
	outputLine("AMOUNT TO TRANSFER: \234");
	return (readInPositiveAmount());
}

//output functions
void UserInterface::showDeletionOfTransactionsUpToDateOnScreen(const double total, const Date date_) const 
{
	cout << "THE " << total << " TRANSACTIONS IN BANK ACCOUNT UP TO DATE " << date_.toFormattedString() << " HAVE BEEN DELETED";
}
void UserInterface::showProduceBalanceOnScreen(double balance) const
{
	cout << "\n      THE CURRENT BALANCE IS: " << fixed << setprecision(2) << setfill(' ') << "\234" << setw(12) << balance;
}
void UserInterface::showWithdrawalOnScreen(bool trAuthorised, double withdrawnAmount) const
{
	if (trAuthorised)
	{
		cout << "\n      TRANSACTION AUTHORISED. \234" << setw(0) << withdrawnAmount << " WITHDRAWN FROM ACCOUNT";
	}
	else
	{
		outputLine("TRANSACTION IMPOSSIBLE!"); // not enough money
	}
}
void UserInterface::showDepositOnScreen(bool trAuthorised, double depositAmount) const
{
	if (trAuthorised)
	{
		cout << "\n      TRANSACTION AUTHORISED. \234" << setw(0) << depositAmount << " DEPOSITED INTO ACCOUNT";
	}
	else
	{
		outputLine("TRANSACTION IMPOSSIBLE!"); // too much to deposit
	}
}
void UserInterface::showTransferOnScreen(bool trOut, bool trIn, double transferAmount, string activeAccountType, string recipientAccountType) const
{
	if (trOut && trIn)
	{
		cout << "\n      TRANSACTION AUTHORISED. \234" << setw(0) << transferAmount << " TRANSFERED FROM ACCOUNT";
	}
	if (!trOut)
	{
		if (activeAccountType == "BANK")
		{
			cout << "\n      TRANSACTION IMPOSSIBLE!";
		}
		else if (activeAccountType == "CURRENT")
		{
			cout << "\n      TRANSACTION UNAUTHORISED. OVERDRAFT LIMIT REACHED. \n";
		} else if (activeAccountType == "CHILD")
		{
			cout << "\n      TRANSACTION UNAUTHORISED. UNABLE TO WITHDRAW FUNDS FROM THIS ACCOUNT TYPE. \n";
		} else if (activeAccountType == "SAVINGS" || "ISA")
		{
			cout << "\n      TRANSACTION UNAUTHORISED. MINIMUM BALANCE REACHED. \n";
		}
	}
	if (!trIn)
	{
		if (recipientAccountType == "CHILD")
		{
			cout << "\n      TRANSACTION UNAUTHORISED. MINIMUM/MAXIMUM DEPOSIT REQUIREMENTS NOT MET.";
		} else if (recipientAccountType == "ISA")
		{
			cout << "\n      TRANSACTION UNAUTHORISED. MAXIMUM YEARLY DEPOSIT REACHED.";
		}
	}
}
void UserInterface::showStatementOnScreen(const string& statement) const {
	outputHeader("PREPARING STATEMENT...");
	cout << statement;
	outputLine("----------------------------------------\n");
}
void UserInterface::showNoTransactionsOnScreen() const{
	cout << "NO TRANSACTIONS IN BANK ACCOUNT";
}

//---------------------------------------------------------------------------
// private support member functions
//---------------------------------------------------------------------------

void UserInterface::showByeScreen() const
{
	outputLine("");
	outputHeader("THANK YOU FOR USING THE ATM");
	endProgram();
}

int UserInterface::readInCommand() const
{
	cout << "\n";
	outputLine("ENTER YOUR COMMAND: ");
	int com;
	cin >> com;
	return com;
}
void UserInterface::showErrorNoValidTransactions(Date date_) const {
	cout << "NO TRANSACTION IN BANK ACCOUNT UP TO DATE " << date_.toFormattedString();
}

void UserInterface::showErrorEmptyAccount() const {
	outputLine("NO TRANSACTIONS IN BANK ACCOUNT");
}
void UserInterface::showErrorInvalidCommand() const
{
	outputLine("INVALID COMMAND CHOICE, TRY AGAIN");
}

void UserInterface::showDeletionCancelled() const {
	outputLine("OPERATION CANCELLED");
}

double UserInterface::readInPositiveAmount() const
{
	double amount;
	cin >> amount;

	while (amount <= 0.0)
	{
		outputLine("AMOUNT SHOULD BE A POSITIVE AMOUNT, TRY AGAIN: ");
		cin >> amount;
	}

	return amount;
}

void UserInterface::outputHeader(const string& header) const
{
	// calculate lengths so we can centre the header
	const int length = header.size();
	const int borderWidth = 40;
	const int leftSpacePad = 6;
	const int paddingRequired = ((borderWidth - length) / 2) + leftSpacePad;

	outputLine("========================================");
	cout << "\n" << setfill(' ') << setw(paddingRequired) << ' ' << header;
	outputLine("========================================");
}

string UserInterface::askForInput(const string& promptForUser) const
{
	outputLine(promptForUser + ": ");
	string userInput;
	cin >> userInput;
	return userInput;
}

void UserInterface::outputLine(const string& text) const
{
	cout << "\n      " << text;
}