//Pascale Vacher - February 18
//OOP Assignment Task 1c - Semester 2
//Group Number: 4
//Team: Laura Booth-Nias	b5008289			Computer Science for Games
//		Freya Jones			b6021099			Computer Science for Games
//		Joshua Edwards		b5019371			Computer Science for Games
//		Johnathan Garland	b6013546			Computer Science for Games

#include "ChildAccount.h"

double ChildAccount::getMaximumPaidIn() const {
	return maximumPaidIn;							

}
double ChildAccount::getMinimumPaidIn() const {
	return minimumPaidIn;

}
double ChildAccount::maxBorrowable() const
{
	return 0;
}
double ChildAccount::maxDepositable() const
{
	return getMaximumPaidIn();
}
double ChildAccount::minDepositable() const
{
	return getMinimumPaidIn();
}



ostream& ChildAccount::putAccountDetailsInStream(ostream& os) const {
	SavingsAccount::putAccountDetailsInStream(os);
	os << minimumPaidIn << "\n";
	os << maximumPaidIn << "\n";
	return os;
}
istream& ChildAccount::getAccountDataFromStream(istream& is) {
	SavingsAccount::getAccountDataFromStream(is);					//get balance
	is >> minimumPaidIn;
	is >> maximumPaidIn;
	return is;
}

const string ChildAccount::prepareFormattedAccountDetails() const
{
	ostringstream os;

	os << SavingsAccount::prepareFormattedAccountDetails();
	os << fixed << setprecision(2);
	os << "\n      MINIMUM PAID IN: \234" << minimumPaidIn;
	os << "\n      MAXIMUM PAID IN: \234" << maximumPaidIn;
	return (os.str());

}

//---------------------------------------------------------------------------
//non-member operator functions
//---------------------------------------------------------------------------

ostream& operator<<(ostream& os, const ChildAccount& aChildAccount) {
	//put (unformatted) CurrentAccount details in stream
	return aChildAccount.putDataInStream(os);
}
istream& operator>>(istream& is, ChildAccount& aChildAccount) {
	//get CurrentAccount details from stream
	return aChildAccount.getDataFromStream(is);
}