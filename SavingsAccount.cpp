
//Pascale Vacher - February 18
//OOP Assignment Task 1c - Semester 2
//Group Number: 4
//Team: Laura Booth-Nias	b5008289			Computer Science for Games
//		Freya Jones			b6021099			Computer Science for Games
//		Joshua Edwards		b5019371			Computer Science for Games
//		Johnathan Garland	b6013546			Computer Science for Games

#include "SavingsAccount.h"

double SavingsAccount::getMinimumBalance() const {
	return minimumBalance;

}
double SavingsAccount::maxBorrowable() const
{
	return BankAccount::getBalance() - getMinimumBalance();
}


ostream& SavingsAccount::putAccountDetailsInStream(ostream& os) const {
	BankAccount::putAccountDetailsInStream(os);
	os << minimumBalance << "\n";

	return os;
}
istream& SavingsAccount::getAccountDataFromStream(istream& is) {
	BankAccount::getAccountDataFromStream(is);					//get balance
	is >> minimumBalance;
	return is;
}
const string SavingsAccount::prepareFormattedAccountDetails() const
{
	ostringstream os;

	os << BankAccount::prepareFormattedAccountDetails();
	os << fixed << setprecision(2);
	os << "\n      MINIMUM BALANCE: \234" << minimumBalance;
	return (os.str());

}


//---------------------------------------------------------------------------
//non-member operator functions
//---------------------------------------------------------------------------

ostream& operator<<(ostream& os, const SavingsAccount& aSavingsAccount) {
	//put (unformatted) CurrentAccount details in stream
	return aSavingsAccount.putDataInStream(os);
}
istream& operator>>(istream& is, SavingsAccount& aSavingsAccount) {
	//get CurrentAccount details from stream
	return aSavingsAccount.getDataFromStream(is);
}