//Pascale Vacher - February 18
//OOP Assignment Task 1c - Semester 2
//Group Number: 4
//Team: Laura Booth-Nias	b5008289			Computer Science for Games
//		Freya Jones			b6021099			Computer Science for Games
//		Joshua Edwards		b5019371			Computer Science for Games
//		Johnathan Garland	b6013546			Computer Science for Games

#ifndef CurrentAccountH
#define CurrentAccountH

#include "BankAccount.h"

using namespace std;

class CurrentAccount : public BankAccount {
public:
	double getOverdraftLimit() const;
	double maxBorrowable() const override;

	ostream& putAccountDetailsInStream(ostream& os) const;
	istream& getAccountDataFromStream(istream& is);

	virtual const string prepareFormattedAccountDetails() const;

private:
	double overdraftLimit;



};

ostream& operator<<(ostream&, const CurrentAccount&);	//output operator
istream& operator>>(istream&, CurrentAccount&);	    //input operator

#endif