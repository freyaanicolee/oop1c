//Pascale Vacher - February 18
//OOP Assignment Task 1c - Semester 2
//Group Number: 4
//Team: Laura Booth-Nias	b5008289			Computer Science for Games
//		Freya Jones			b6021099			Computer Science for Games
//		Joshua Edwards		b5019371			Computer Science for Games
//		Johnathan Garland	b6013546			Computer Science for Games

#ifndef ISAAccountH
#define ISAAccountH

#include "SavingsAccount.h"

using namespace std;

class ISAAccount : public SavingsAccount {
public:

	ISAAccount();

	double maxDepositable() const;
	double maxBorrowable() const;

	double getMaximumYearlyDeposit() const;
	double getCurrentYearlyDeposit() const;
	Date getEndDepositPeriod() const;

	ostream& putAccountDetailsInStream(ostream& os) const;
	istream& getAccountDataFromStream(istream& is);

	virtual const string prepareFormattedAccountDetails() const;
private:


	void updateCurrentYearlyDeposit(double) ;
	Date endDepositPeriod;
	double currentYearlyDeposit;
	double maximumYearlyDeposit;





};
ostream& operator<<(ostream&, const ISAAccount&);	//output operator
istream& operator>>(istream&, ISAAccount&);	    //input operator

#endif