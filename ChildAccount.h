//Pascale Vacher - February 18
//OOP Assignment Task 1c - Semester 2
//Group Number: 4
//Team: Laura Booth-Nias	b5008289			Computer Science for Games
//		Freya Jones			b6021099			Computer Science for Games
//		Joshua Edwards		b5019371			Computer Science for Games
//		Johnathan Garland	b6013546			Computer Science for Games

#ifndef ChildAccountH
#define ChildAccountH

#include "SavingsAccount.h"

using namespace std;

class ChildAccount : public SavingsAccount {
public:
	double getMaximumPaidIn() const;
	double getMinimumPaidIn() const;
	double maxBorrowable() const;
	double maxDepositable() const;
	double minDepositable() const;

	ostream& putAccountDetailsInStream(ostream& os) const;
	istream& getAccountDataFromStream(istream& is);


	virtual const string prepareFormattedAccountDetails() const;
private:
	double maximumPaidIn;
	double minimumPaidIn;




};

ostream& operator<<(ostream&, const SavingsAccount&);	//output operator
istream& operator>>(istream&, SavingsAccount&);	    //input operator

#endif