//Pascale Vacher - February 18
//OOP Assignment Task 1c - Semester 2
//Group Number: 4
//Team: Laura Booth-Nias	b5008289			Computer Science for Games
//		Freya Jones			b6021099			Computer Science for Games
//		Joshua Edwards		b5019371			Computer Science for Games
//		Johnathan Garland	b6013546			Computer Science for Games

#include "ATM.h"

//---------------------------------------------------------------------------
//ATM: class implementation
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//public member functions
//---------------------------------------------------------------------------

//____constructors & destructors

ATM::ATM()
	: p_theActiveAccount_(nullptr), p_theCard_(nullptr)
{}

ATM::~ATM()
{
	assert(p_theActiveAccount_ == nullptr);
	assert(p_theCard_ == nullptr);
}

//____other public member functions

void ATM::activateCashPoint() {
	int command(theUI_.showMainMenuAndGetCommand());
	while (command != QUIT_COMMAND)
	{
		executeCardCommand(command);
		theUI_.wait();
		command = theUI_.showMainMenuAndGetCommand();
	}
	theUI_.showByeScreen();
}

//---------------------------------------------------------------------------
//private support member functions
//---------------------------------------------------------------------------

void ATM::executeCardCommand(int option) {
	switch (option)
	{
	case 1:
	{
		string cardNum(theUI_.readInCardToBeProcessed());
		string cardFilename(theUI_.cardFilename(cardNum));
		int validCardCode(validateCard(cardFilename));
		theUI_.showValidateCardOnScreen(validCardCode, cardNum);
		if (validCardCode == VALID_CARD)
		{
			//dynamically create a card and store card data
			activateCard(cardFilename);

			//select request for active card 
			int option = theUI_.showCardMenuAndGetCommand(cardNum);
			while (option != QUIT_COMMAND)
			{
				switch (option)
				{
				case 1: m_card1_manageIndividualAccount();
					break;
				case 2: m_card1_showFundsAvailableOnAllAccounts();
					break;
				default:
					theUI_.showErrorInvalidCommand();
				}

				theUI_.wait();
				option = theUI_.showCardMenuAndGetCommand(cardNum);
			}

			//free memory space used by active card
			releaseCard();
		}
		break;
	}
	default:theUI_.showErrorInvalidCommand();
	}
}
void ATM::m_card1_manageIndividualAccount() {
	assert(p_theCard_ != nullptr);
	theUI_.showCardAccounts(p_theCard_->getCardNumber(), p_theCard_->toFormattedString());
	executeAccountCommand();
}
void ATM::m_card1_showFundsAvailableOnAllAccounts() {
	assert(p_theCard_ != nullptr);
	string mad;
	double m(0);
	List<string> accts = p_theCard_->getAccountsList();
	bool empty = accts.isEmpty();

	while (!accts.isEmpty())
	{
		//pointer to the account file
		BankAccount* pacct = activateAccount(theUI_.accountFilename(accts.first()));	
		//store balance in m
		m += pacct->maxBorrowable();								
		//store balance and account number as string
		mad += pacct->prepareFormattedMiniAccountDetails();					
		//write to file 
		pacct = releaseAccount(pacct, theUI_.accountFilename(accts.first()));			
		//delete first element in account
		accts.deleteFirst();															
	}
	theUI_.showFundsAvailableOnScreen(empty, mad, m);
}
int ATM::validateCard(const string& filename) const {
	//check that the card exists (valid)
	if (!canOpenFile(filename))   //invalid card
		return UNKNOWN_CARD;
	else
		//card empty (exist but no bank account listed on card)
		if (!accountsListedOnCard(filename))
			return EMPTY_CARD;
		else
			//card valid (exists and linked to at least one bank account)
			return VALID_CARD;
}
int ATM::validateAccount(const string& filename, bool secondAcc) const {
	//check that the account is valid 
	//NOTE: MORE WORK NEEDED here in case of transfer
	// Is there a second account? Is that second account the same as the Active.
	if (secondAcc)
	{
		string activeAccount(theUI_.accountFilename(p_theActiveAccount_->getAccountNumber()));
		if (filename == activeAccount)
			return ACTIVE_ACCOUNT;
	} 
	if (!canOpenFile(filename))
		//account does not exist
		return UNKNOWN_ACCOUNT;
	else
		//account type not recognised
		if (BankAccount::getAccountType(filename) == "UNKNOWN")
			//if (getAccountTypeCode(filename) == UNKNOWN_ACCOUNT_TYPE)
			return INVALID_ACCOUNT_TYPE;
		else
			//unaccessible account (exists but not listed on card)
			if (!p_theCard_->onCard(filename))
				return UNACCESSIBLE_ACCOUNT;
			else
				//account valid (exists and accessible)
				return VALID_ACCOUNT;
}
void ATM::executeAccountCommand() {
	assert(p_theActiveAccount_ == nullptr);
	//process from one account
	//select active account and check that it is valid
	const string anAccountNumber(theUI_.readInAccountToBeProcessed());
	const string bankAccountFilename(theUI_.accountFilename(anAccountNumber));
	const int validAccountCode(validateAccount(bankAccountFilename, false));
	theUI_.showValidateAccountOnScreen(validAccountCode, anAccountNumber);
	if (validAccountCode == VALID_ACCOUNT) //valid account: exists, accessible with card and not already open
	{
		//dynamically create a bank account to store data from file
		p_theActiveAccount_ = activateAccount(bankAccountFilename);

		//select request for active account 
		int option = theUI_.showAccountMenuAndGetCommand(p_theActiveAccount_->getAccountNumber());
		while (option != QUIT_COMMAND)
		{
			switch (option)
			{
			case 1:	m_acct1_produceBalance();
				break;
			case 2: m_acct2_withdrawFromBankAccount();
				break;
			case 3:	m_acct3_depositToBankAccount();
				break;
			case 4:	m_acct4_produceStatement();
				break;
			case 5: m_acct5_showAllDepositsTransactions();
				break;
			case 6: m_acct6_showMiniStatement();
				break;
			case 7: m_acct7_searchForTransactions();
				break;
			case 8: m_acct8_clearTransactionsUpToDate();
				break;
			case 9: m_acct9_transferFunds();
				break;
			default:theUI_.showErrorInvalidCommand();
			}
			theUI_.wait();
			option = theUI_.showAccountMenuAndGetCommand(p_theActiveAccount_->getAccountNumber());   //select another option
		}

		//store new state of bank account in file and free bank account memory space
		p_theActiveAccount_ = releaseAccount(p_theActiveAccount_, bankAccountFilename);
	}
}

//------ menu options
//---option 1
void ATM::m_acct1_produceBalance() const {
	assert(p_theActiveAccount_ != nullptr);
	double balance(p_theActiveAccount_->getBalance());
	theUI_.showProduceBalanceOnScreen(balance);
}
//---option 2
void ATM::m_acct2_withdrawFromBankAccount() {
	assert(p_theActiveAccount_ != nullptr);
	double amountToWithdraw(theUI_.readInWithdrawalAmount());
	bool transactionAuthorised(p_theActiveAccount_->canWithdraw(amountToWithdraw));
	if (transactionAuthorised)
	{   //transaction is accepted: money can be withdrawn from account
		p_theActiveAccount_->recordWithdrawal(amountToWithdraw);
	}   //else do nothing
	theUI_.showWithdrawalOnScreen(transactionAuthorised, amountToWithdraw);
}
//---option 3
void ATM::m_acct3_depositToBankAccount() {
	assert(p_theActiveAccount_ != nullptr);
	double amountToDeposit(theUI_.readInDepositAmount());
	bool transactionAuthorised(p_theActiveAccount_->canDeposit(amountToDeposit));
	if (transactionAuthorised){
		p_theActiveAccount_->recordDeposit(amountToDeposit);
	}
	theUI_.showDepositOnScreen(transactionAuthorised, amountToDeposit);
}
//---option 4
void ATM::m_acct4_produceStatement() const {
	assert(p_theActiveAccount_ != nullptr);
	theUI_.showStatementOnScreen(p_theActiveAccount_->prepareFormattedStatement());
}
//---option 5
void ATM::m_acct5_showAllDepositsTransactions() const {
	assert(p_theActiveAccount_ != nullptr);
	string str;
	double totalTransactions(0);
	bool noTransaction = p_theActiveAccount_->isEmptyTransactionList();

	if (!noTransaction)
	{
		p_theActiveAccount_->produceAllDepositTransactions(totalTransactions, str);
	}

	theUI_.showAllDepositsOnScreen(noTransaction, str, totalTransactions);
}
//---option 6
void ATM::m_acct6_showMiniStatement() const {
	assert(p_theActiveAccount_ != nullptr);
	string str;
	double total = 0;
	bool isEmpty = p_theActiveAccount_->isEmptyTransactionList();

	if (!isEmpty)
	{
		//ask how many transactions to see
		total = theUI_.readInNumberOfTransactions();
		//show the number of transactions asked for in the account we're using
		str = p_theActiveAccount_->produceNMostRecentTransactions(total);	

	}
	//store balance and account number as string
	string mad = p_theActiveAccount_->prepareFormattedMiniAccountDetails(); 
	theUI_.showMiniStatementOnScreen(isEmpty, total, mad + str);			
}
//--option 7
void ATM::m_acct7_searchForTransactions() const {
	assert(p_theActiveAccount_ != nullptr);
	bool isEmpty = p_theActiveAccount_->isEmptyTransactionList();
	if (isEmpty) {
		theUI_.showNoTransactionsOnScreen();
	}
	else {
		searchForTransactions();
	}
}
// --- option 8
void ATM::m_acct8_clearTransactionsUpToDate()
{
	assert(p_theActiveAccount_ != nullptr);
	bool isEmpty = p_theActiveAccount_->isEmptyTransactionList();
	if (!isEmpty) {
		double size_;
		string transactionsStr_;
		Date cDate_ = p_theActiveAccount_->getCreationDate();
		Date date_ = theUI_.readInValidDate(cDate_);
		p_theActiveAccount_->produceTransactionsUpToDate(date_, size_, transactionsStr_);
		if (size_ != 0){
			theUI_.showTransactionsUpToDateOnScreen(isEmpty, size_, transactionsStr_);
			if (theUI_.readInConfirmDeletion())
			{
				p_theActiveAccount_->recordDeletionOfTransactionUpToDate(date_);
				theUI_.showDeletionOfTransactionsUpToDateOnScreen(size_, date_);
			}
			else {
				theUI_.showDeletionCancelled();
			}
		}
		else {
			theUI_.showErrorNoValidTransactions(date_);
		}
	}
	else {
		theUI_.showErrorEmptyAccount();
	}
}
//---option 9
void ATM::m_acct9_transferFunds() {
	assert(p_theActiveAccount_ != nullptr);

	// Pointer for the account to recieve funds.
	BankAccount* p_recipientAccount_;

	// Asks User for account number to recieve funds and displays all accounts on current card.
	theUI_.showCardAccounts(p_theCard_->getCardNumber(), p_theCard_->toFormattedString());
	const string anAccountNumber(theUI_.readInAccountToRecieveFunds());
	const string bankAccountFilename(theUI_.accountFilename(anAccountNumber));
	const int validAccountCode(validateAccount(bankAccountFilename, true));
	
	theUI_.showValidateAccountOnScreen(validAccountCode, anAccountNumber);

	// Valid account: exists, accessible with card and not already open.
	if (validAccountCode == VALID_ACCOUNT) 
	{
			// Dynamically create a new bank account to store data from the recipient account file.
			p_recipientAccount_ = activateAccount(bankAccountFilename);

			attemptTransfer(p_recipientAccount_);

			//store new state of bank account in file and free bank account memory space
			p_recipientAccount_ = releaseAccount(p_recipientAccount_, bankAccountFilename);
	}
}

//------private file functions
void ATM::searchForTransactions() const
{
	theUI_.showSearchMenu();
	int opt = theUI_.readInSearchCommand();
	switch (opt)
	{
	case 1:
		m_trl1_showTransactionForAmount();
		break;
	case 2:
		m_trl1_showTransactionForTitle();
		break;
	case 3:
		m_trl1_showTransactionForDate();
		break;
	}
}
//option1
void ATM::m_trl1_showTransactionForAmount() const{
	double a = theUI_.readInAmount();
	int n = 0;
	string str =  p_theActiveAccount_->produceTransactionForAmount(a , n);
	theUI_.showMatchingTransactionsOnScreenDouble(a, n,str);
}
//option2
void ATM::m_trl1_showTransactionForTitle() const{
	string title = theUI_.readInTitle();
	int n = 0;
	string str = p_theActiveAccount_->produceTransactionForTitle(title, n);
	theUI_.showMatchingTransactionsOnScreenString(title, n, str);
}//option3
void ATM::m_trl1_showTransactionForDate() const{
	Date date = theUI_.readInValidDate(p_theActiveAccount_->getCreationDate());
	int n = 0;
	string str = p_theActiveAccount_->produceTransactionForDate(date, n);
	theUI_.showMatchingTransactionsOnScreenDate(date, n, str);
}

bool ATM::canOpenFile(const string& filename) const {
	//check if a file already exist
	ifstream inFile;
	inFile.open(filename.c_str(), ios::in); 	//open file
												//if does not exist fail() return true
	return (!inFile.fail());	//close file automatically when inFile goes out of scope
}

bool ATM::accountsListedOnCard(const string& cashCardFileName) const {
	//check that card is linked with account data
	ifstream inFile;
	inFile.open(cashCardFileName.c_str(), ios::in); 	//open file
	assert(!inFile.fail()); //file should exist at this point 
							//check that it contains some info in addition to card number
	string temp;
	inFile >> temp; //read card number
	inFile >> temp;	//ready first account data or eof if end of file found
	return (!inFile.eof());
}

void ATM::activateCard(const string& filename) {
	//dynamically create a cash card to store data from file
	//effectively create the cash card instance with the data
	assert(p_theCard_ == nullptr);
	p_theCard_ = new Card;
	assert(p_theCard_ != nullptr);
	p_theCard_->readInCardFromFile(filename);
}

void ATM::releaseCard() {
	//release the memory allocated to the dynamic instance of a card
	delete p_theCard_;
	p_theCard_ = nullptr;
}

void ATM::attemptTransfer(BankAccount* p_recipient_)
{
	// Can Transfer out and in accounts.
	bool trOutOK, trInOK;
	string aAccountType(p_theActiveAccount_->getAccountType(p_theActiveAccount_->getAccountNumber()[0]));
	string rAccountType(p_recipient_->getAccountType(p_recipient_->getAccountNumber()[0]));

	// Read in amount to be Transfered.
	double amountToTransfer(theUI_.readInTransferAmount());

	// Collects booleans for Transfers in/out.
	trOutOK = (p_theActiveAccount_->canTransferOut(amountToTransfer));
	trInOK = (p_recipient_->canTransferIn(amountToTransfer));
	
	// Argument to check whether Transfer is possible for both accounts.
	if (trOutOK && trInOK)
	{   //transaction is accepted: money can be transfered from account
		p_theActiveAccount_->recordTransfer(amountToTransfer, p_recipient_);
	}   

	theUI_.showTransferOnScreen(trOutOK, trInOK, amountToTransfer, aAccountType, rAccountType);
}

//static member function
char ATM::getAccountTypeCode(const string& filename) {
	//(simply) identify type/class of account from the account number
	//starts with 0 for bank account, 1 for current account, 2 for saving account, etc.
	return filename[13]; //14th char from the filename ("data/account_101.txt")
}

BankAccount* ATM::activateAccount(const string& filename) {
	//Pre-condition: type of the account is valid
	assert(BankAccount::getAccountType(filename) != "UNKNOWN");
	//effectively create the active bank account instance of the appropriate class
	//and store the appropriate data read from the file
	BankAccount* p_BA(nullptr);
	//made BankAccount and SavingsAccount abstract so not needed here
	switch (getAccountTypeCode(filename))
	{
	case CURRENTACCOUNT_TYPE:
		p_BA = new CurrentAccount;
		break;
	case CHILDACCOUNT_TYPE:
		p_BA = new ChildAccount;
		break;
	case ISAACCOUNT_TYPE:
		p_BA = new ISAAccount;
		break;
	}
	p_BA->readInBankAccountFromFile(filename); //read account details from file
											   //use dynamic memory allocation: the bank account created will have to be released in releaseAccount
	return p_BA;
}

BankAccount* ATM::releaseAccount(BankAccount* p_BA, string filename) {
	//store (possibly updated) data back in file
	assert(p_BA != nullptr);
	p_BA->storeBankAccountInFile(filename);
	//effectively destroy the bank account instance
	delete p_BA;
	return nullptr;
}