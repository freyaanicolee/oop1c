//Pascale Vacher - February 18
//OOP Assignment Task 1c - Semester 2
//Group Number: 4
//Team: Laura Booth-Nias	b5008289			Computer Science for Games
//		Freya Jones			b6021099			Computer Science for Games
//		Joshua Edwards		b5019371			Computer Science for Games
//		Johnathan Garland	b6013546			Computer Science for Games

#include "CurrentAccount.h"

double CurrentAccount::getOverdraftLimit() const {
	return overdraftLimit;

}

double CurrentAccount::maxBorrowable() const {
	double borrow = 0;
	if (BankAccount::getBalance() > -getOverdraftLimit())
	{
		borrow = BankAccount::getBalance() + getOverdraftLimit();
	}
	
	return borrow;

}

ostream& CurrentAccount::putAccountDetailsInStream(ostream& os) const {
	BankAccount::putAccountDetailsInStream(os);
	os << overdraftLimit << "\n";
	return os;
}
istream& CurrentAccount::getAccountDataFromStream(istream& is) {
	BankAccount::getAccountDataFromStream(is);					//get balance
	is >> overdraftLimit;
	return is;
}

const string CurrentAccount::prepareFormattedAccountDetails() const
{
	ostringstream os;

	os << BankAccount::prepareFormattedAccountDetails();
	os << fixed << setprecision(2);
	os << "\n      OVERDRAFT LIMIT: \234" << overdraftLimit;
	return (os.str());

}
//---------------------------------------------------------------------------
//non-member operator functions
//---------------------------------------------------------------------------

ostream& operator<<(ostream& os, const CurrentAccount& aCurrentAccount) {
	//put (unformatted) CurrentAccount details in stream
	return aCurrentAccount.putDataInStream(os);
}
istream& operator>>(istream& is, CurrentAccount& aCurrentAccount) {
	//get CurrentAccount details from stream
	return aCurrentAccount.getDataFromStream(is);
}