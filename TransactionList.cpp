//Pascale Vacher - February 18
//OOP Assignment Task 1c - Semester 2
//Group Number: 4
//Team: Laura Booth-Nias	b5008289			Computer Science for Games
//		Freya Jones			b6021099			Computer Science for Games
//		Joshua Edwards		b5019371			Computer Science for Games
//		Johnathan Garland	b6013546			Computer Science for Games


#include "TransactionList.h"

//---------------------------------------------------------------------------
//TransactionList: class implementation
//---------------------------------------------------------------------------

//____public member functions

//____constructors & destructors

//____other public member functions

void TransactionList::addNewTransaction(const Transaction& tr) {
	listOfTransactions_.addInFront(tr);
}
const Transaction TransactionList::newestTransaction() const {
	return (listOfTransactions_.first());
}
const TransactionList TransactionList::olderTransactions() const {
	TransactionList trlist(*this);
	trlist.deleteFirstTransaction();
	return trlist;
}
void TransactionList::deleteFirstTransaction() {
	listOfTransactions_.deleteFirst();
}
void TransactionList::deleteGivenTransaction(const Transaction& tr) {
	listOfTransactions_.deleteOne(tr);
}
void TransactionList::deleteTransactionsUpToDate(const Date date_,  TransactionList temp)
{
	if (date_ < newestTransaction().getDate()) {
		temp.addNewTransaction(newestTransaction());
		deleteFirstTransaction();
		deleteTransactionsUpToDate(date_, temp);
	}
	*this = temp;
}
int TransactionList::size() const {
	return (listOfTransactions_.length());
}
const TransactionList TransactionList::getAllDepositTransactions() const {
	//only ones that are positive
	TransactionList trlist(*this);
	return trlist;
}
const TransactionList TransactionList::getMostRecentTransactions(int x) const {
	TransactionList trlist(*this);
	TransactionList recentTr;
	
	if (!trlist.size() == 0) {
		for (int i = 0; i < x; i++)
		{
			recentTr.addNewTransaction(trlist.newestTransaction());
			trlist = trlist.olderTransactions();
		}
	}
	return recentTr;
}
TransactionList TransactionList::getTransactionsUpToDate(Date date_) {
	if (getTotalTransactions() == 0)
		return *this;
	if (date_< newestTransaction().getDate()) {
		deleteFirstTransaction();

		return getTransactionsUpToDate(date_);
	}
	return *this;
}
const double TransactionList::getTotalTransactions() const {

	return size();
}
const TransactionList TransactionList::getTransactionsForAmount(double amount) const
{
	TransactionList trlist(*this);
	TransactionList recentTr;
	int size =  trlist.getTotalTransactions();
	if (!trlist.size() == 0) {
		for (int i = 0; i < size; i++)
		{
			if (trlist.newestTransaction().getAmount() == amount)
			{
				recentTr.addNewTransaction(trlist.newestTransaction());
			}
			trlist = trlist.olderTransactions();
		}
	}
	return recentTr;
}
const TransactionList TransactionList::getTransactionsForTitle(string title) const
{
	TransactionList trlist(*this);
	TransactionList recentTr;

	if (!trlist.size() == 0) {
		for (int i = 0; i < trlist.getTotalTransactions(); i++)
		{
			if (trlist.newestTransaction().getTitle() == title)
			{
				recentTr.addNewTransaction(trlist.newestTransaction());
			}
			trlist = trlist.olderTransactions();
		}
	}
	return recentTr;
}
const TransactionList TransactionList::getTransactionsForDate(Date date) const
{
	TransactionList trlist(*this);
	TransactionList recentTr;

	if (!trlist.size() == 0) {
		for (int i = 0; i < trlist.getTotalTransactions(); i++)
		{
			if (trlist.newestTransaction().getDate() == date)
			{
				recentTr.addNewTransaction(trlist.newestTransaction());
			}
			trlist = trlist.olderTransactions();
		}
	}
	return recentTr;
}

const string TransactionList::toFormattedString() const {
	//return transaction list as a (formatted) string
	ostringstream os_transactionlist;
	TransactionList tempTrList(*this);
	while (!(tempTrList.size() == 0))
	{
		os_transactionlist << endl << tempTrList.newestTransaction().toFormattedString();
		tempTrList.deleteFirstTransaction();
	}
	return (os_transactionlist.str());
}

ostream& TransactionList::putDataInStream(ostream& os) const {
	//put (unformatted) transaction list into an output stream
	TransactionList tempTrList(*this);
	while (!(tempTrList.size() == 0))
	{
		os << tempTrList.newestTransaction() << endl;
		tempTrList.deleteFirstTransaction();
	}
	return os;
}
istream& TransactionList::getDataFromStream(istream& is) {
	//read in (unformatted) transaction list from input stream
	Transaction aTransaction;
	is >> aTransaction;	//read first transaction
	while (is) 	//while not end of file
	{
		listOfTransactions_.addAtEnd(aTransaction);   //add transaction to list of transactions
		is >> aTransaction;	//read in next transaction
	}
	return is;
}


//---------------------------------------------------------------------------
//non-member operator functions
//---------------------------------------------------------------------------

ostream& operator<<(ostream& os, const TransactionList& aTransactionList) {
	return (aTransactionList.putDataInStream(os));
}
istream& operator>>(istream& is, TransactionList& aTransactionList) {
	return (aTransactionList.getDataFromStream(is));
}
