//Pascale Vacher - February 18
//OOP Assignment Task 1c - Semester 2
//Group Number: 4
//Team: Laura Booth-Nias	b5008289			Computer Science for Games
//		Freya Jones			b6021099			Computer Science for Games
//		Joshua Edwards		b5019371			Computer Science for Games
//		Johnathan Garland	b6013546			Computer Science for Games


#include "ATM.h"         //include modules header files

//---------------------------------------------------------------------------

//main application

int main() { 
	//create the application
	ATM theATM; // default constructor called here
    //run it
	theATM.activateCashPoint();
	//destroy it - destructor called here
	return 0;
}
