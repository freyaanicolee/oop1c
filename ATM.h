//Pascale Vacher - February 18
//OOP Assignment Task 1c - Semester 2
//Group Number: 4
//Team: Laura Booth-Nias	b5008289			Computer Science for Games
//		Freya Jones			b6021099			Computer Science for Games
//		Joshua Edwards		b5019371			Computer Science for Games
//		Johnathan Garland	b6013546			Computer Science for Games


#ifndef ATMH
#define ATMH 

//---------------------------------------------------------------------------
//ATM: class declaration
//---------------------------------------------------------------------------

#include "Card.h"
#include "BankAccount.h"
#include "CurrentAccount.h"
#include "ChildAccount.h"
#include "SavingsAccount.h"
#include "ISAAccount.h"
#include "UserInterface.h"
#include <iostream>
#include <fstream>
#include <string>
#include <cassert>
using namespace std;

class ATM {
public:
	ATM();	//default constructor
	~ATM();	//destructor
	void activateCashPoint();
private:
	//data items
	BankAccount* p_theActiveAccount_;
	Card* p_theCard_;

	//access the UI instance
	UserInterface& theUI_ = *UserInterface::getInstance();

	//support functions
	int  validateCard(const string&) const;
	void executeCardCommand(int);
	int  validateAccount(const string&, bool) const;
	void executeAccountCommand();
	void searchForTransactions() const;

	//card menu commands
	void m_card1_manageIndividualAccount();
	void m_card1_showFundsAvailableOnAllAccounts();
	//account menu commands
	void m_acct1_produceBalance() const;
	void m_acct2_withdrawFromBankAccount();
	void m_acct3_depositToBankAccount();
	void m_acct4_produceStatement() const;
	void m_acct5_showAllDepositsTransactions() const;
	void m_acct6_showMiniStatement() const;
	void m_acct7_searchForTransactions() const;
	void m_acct8_clearTransactionsUpToDate();
	void m_acct9_transferFunds();
	//transaction search
	void m_trl1_showTransactionForAmount() const;
	void m_trl1_showTransactionForTitle() const;
	void m_trl1_showTransactionForDate() const;

	//support file handling functions and creation of dynamic objects
	bool canOpenFile(const string&) const;
	static char getAccountTypeCode(const string&);
	bool accountsListedOnCard(const string&) const;

	void activateCard(const string&);
	void releaseCard();

	void attemptTransfer(BankAccount*);

	BankAccount* activateAccount(const string&);
	BankAccount* releaseAccount(BankAccount*, string);
};

#endif
