//Pascale Vacher - February 18
//OOP Assignment Task 1c - Semester 2
//Group Number: 4
//Team: Laura Booth-Nias	b5008289			Computer Science for Games
//		Freya Jones			b6021099			Computer Science for Games
//		Joshua Edwards		b5019371			Computer Science for Games
//		Johnathan Garland	b6013546			Computer Science for Games

#include "ISAAccount.h"

ISAAccount::ISAAccount()
{
	
}

double ISAAccount::maxDepositable() const
{
	double deposit;
	Date current = Date::currentDate();
	Date endDep = getEndDepositPeriod();
	if ((current < endDep))
	{
		deposit = getMaximumYearlyDeposit() - getCurrentYearlyDeposit();
	}
	else
	{
		deposit = 0;
	}
	return deposit;
}
double ISAAccount::maxBorrowable() const
{
	double borrow;
	Date current = Date::currentDate();
	Date endDep = getEndDepositPeriod();
	borrow = BankAccount::getBalance() - SavingsAccount::getMinimumBalance();

	return borrow;
}



double ISAAccount::getMaximumYearlyDeposit() const
{
	return maximumYearlyDeposit;
}

double ISAAccount::getCurrentYearlyDeposit() const
{
	return currentYearlyDeposit;
}

Date ISAAccount::getEndDepositPeriod() const
{
	return Date(SavingsAccount::getCreationDate().getDay(), SavingsAccount::getCreationDate().getMonth(), SavingsAccount::getCreationDate().getYear() + 1);
}

void ISAAccount::updateCurrentYearlyDeposit(double amount)
{
	currentYearlyDeposit += amount;
}



//
ostream& ISAAccount::putAccountDetailsInStream(ostream& os) const {
	SavingsAccount::putAccountDetailsInStream(os);
	os << currentYearlyDeposit<<"\n";
	os << maximumYearlyDeposit << "\n";
	return os;
}

istream& ISAAccount::getAccountDataFromStream(istream& is) {
	SavingsAccount::getAccountDataFromStream(is);					//get balance
	
	is >> currentYearlyDeposit;
	is >> maximumYearlyDeposit;

	return is;
}

const string ISAAccount::prepareFormattedAccountDetails() const
{
	ostringstream os;

	os << SavingsAccount::prepareFormattedAccountDetails();
	os << fixed << setprecision(2);
	os << "\n      MAXIMUM YEARLY DEPOSIT: \234" << maximumYearlyDeposit;
	os << "\n      MINIMUM BALANCE: \234" << SavingsAccount::getMinimumBalance();
	os << "\n      CURRENT YEARLY DEPOSIT: \234" << currentYearlyDeposit;
	return (os.str());

}

//---------------------------------------------------------------------------
//non-member operator functions
//---------------------------------------------------------------------------

ostream& operator<<(ostream& os, const ISAAccount& aISAAccount) {
	//put (unformatted) CurrentAccount details in stream
	return aISAAccount.putDataInStream(os);
}
istream& operator>>(istream& is, ISAAccount& aISAAccount) {
	//get CurrentAccount details from stream
	return aISAAccount.getDataFromStream(is);
}