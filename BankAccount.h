//Pascale Vacher - February 18
//OOP Assignment Task 1c - Semester 2
//Group Number: 4
//Team: Laura Booth-Nias	b5008289			Computer Science for Games
//		Freya Jones			b6021099			Computer Science for Games
//		Joshua Edwards		b5019371			Computer Science for Games
//		Johnathan Garland	b6013546			Computer Science for Games


#ifndef BankAccountH
#define BankAccountH

//---------------------------------------------------------------------------
//BankAccount: class declaration
//---------------------------------------------------------------------------

//#include "Date.h"
//#include "Transaction.h"
#include "TransactionList.h"
#include "Constants.h"

#include <fstream>
#include <cassert>
using namespace std;


class BankAccount {
public:
	//constructors & destructor
	BankAccount();
	~BankAccount();

	//getter (assessor) functions
	const string getAccountNumber() const;
	const Date getCreationDate() const;
	double getBalance() const;
	const TransactionList getTransactions() const;
	bool	isEmptyTransactionList() const;

	//other operations
	const string prepareFormattedStatement() const;

	void recordDeposit(double amount);

	virtual double maxBorrowable() const;
	virtual double maxDepositable() const;
	virtual double minDepositable() const;
	virtual bool canWithdraw(double amount) const;
	virtual bool canDeposit(double amount) const;
	void recordWithdrawal(double amount);

	void produceTransactionsUpToDate(const Date date_, double& total, string& str);
	string produceTransactionForAmount(double&, int&);
	string produceTransactionForTitle(string&, int&);
	string produceTransactionForDate(Date&, int&);
	void produceAllDepositTransactions(double&, string);
	string produceNMostRecentTransactions(double&);
	void readInBankAccountFromFile(const string& fileName);
	void storeBankAccountInFile(const string& fileName) const;
	//functions to put data into and get data from streams
	virtual ostream& putDataInStream(ostream& os) const;
	virtual ostream& putAccountDetailsInStream(ostream& os) const;
	virtual istream& getDataFromStream(istream& is);
	virtual istream& getAccountDataFromStream(istream& is);

	// Option 9: Transferal of Funds.
	bool canTransferOut(double amount) const;
	bool canTransferIn(double amount) const;
	void recordTransfer(double amount, BankAccount* recipient);
	void recordTransferOut(double amount, string accountNo, BankAccount* recipient);
	void recordTransferIn(double amount, string accountNo, BankAccount* recipient);

	void recordDeletionOfTransactionUpToDate(const Date date_);

	//pure virtual function - abstract
	virtual const string prepareFormattedAccountDetails() const = 0;

	const string prepareFormattedTransactionList() const;
	const string prepareFormattedMiniAccountDetails() const;

	static const string getAccountType(const string& filename);
	static const string getAccountType(char n);

private:
	//data items
	string accountNumber_;
	Date   creationDate_;
	double balance_;
	TransactionList transactions_;

	//support functions
	void updateBalance(double amount);
	virtual ostream& putTransactionInStream(ostream& os) const;
	virtual istream& getTransactionsDataFromStream(istream& is);
};

//---------------------------------------------------------------------------
//non-member operator functions
//---------------------------------------------------------------------------

ostream& operator<<(ostream&, const BankAccount&);	//output operator
istream& operator>>(istream&, BankAccount&);	    //input operator

#endif
