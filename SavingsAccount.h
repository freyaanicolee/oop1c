//Pascale Vacher - February 18
//OOP Assignment Task 1c - Semester 2
//Group Number: 4
//Team: Laura Booth-Nias	b5008289			Computer Science for Games
//		Freya Jones			b6021099			Computer Science for Games
//		Joshua Edwards		b5019371			Computer Science for Games
//		Johnathan Garland	b6013546			Computer Science for Games


#ifndef SavingsAccountH
#define SavingsAccountH

#include "BankAccount.h"

using namespace std;

class SavingsAccount : public BankAccount {
public:
	double getMinimumBalance() const;
	virtual double maxBorrowable() const;
	ostream& putAccountDetailsInStream(ostream& os) const;
	istream& getAccountDataFromStream(istream& is);

	//pure virtual function - abstract
	virtual const string prepareFormattedAccountDetails() const = 0;
private:
	double minimumBalance;


};

ostream& operator<<(ostream&, const SavingsAccount&);	//output operator
istream& operator>>(istream&, SavingsAccount&);	    //input operator
#endif