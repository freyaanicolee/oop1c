//Pascale Vacher - February 18
//OOP Assignment Task 1c - Semester 2
//Group Number: 4
//Team: Laura Booth-Nias	b5008289			Computer Science for Games
//		Freya Jones			b6021099			Computer Science for Games
//		Joshua Edwards		b5019371			Computer Science for Games
//		Johnathan Garland	b6013546			Computer Science for Games


#ifndef UserInterfaceH 
#define UserInterfaceH

//---------------------------------------------------------------------------
//UserInterface: class declaration
//---------------------------------------------------------------------------

#include "constants.h"
#include "time.h"
#include "date.h"
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
using namespace std;

class UserInterface {
public:

	static UserInterface* getInstance();

	void showByeScreen() const;

	int showMainMenuAndGetCommand() const;
	int showCardMenuAndGetCommand(const string& cardNum) const;
	int showAccountMenuAndGetCommand(const string& accNum) const;

	void showDeletionCancelled() const;
	void showErrorNoValidTransactions(const Date date_) const;
	void showErrorEmptyAccount() const;
	void showErrorInvalidCommand() const;
	void wait() const;
	void endProgram() const;
	const string readInCardToBeProcessed() const;
	void showValidateCardOnScreen(int validCode, const string& cardNum) const;
	void showCardAccounts(const string& cardNum, const string& cardSt) const;
	const string readInAccountToBeProcessed() const;
	void showValidateAccountOnScreen(int valid, const string& acctNum) const;

	string readInAccountToRecieveFunds() const;
	double readInTransferAmount() const;

	static const string cardFilename(const string& cn);
	static const string accountFilename(const string& an);

	Date readInValidDate(const Date Cdate) const;

	bool readInConfirmDeletion() const;
	double readInWithdrawalAmount() const;
	double readInDepositAmount() const;
	int readInNumberOfTransactions() const;
	int readInSearchCommand() const;
	int readInAmount() const;
	string readInTitle() const;

	void showProduceBalanceOnScreen(double bal) const;
	void showDepositOnScreen(bool auth, double deposit) const;
	void showWithdrawalOnScreen(bool auth, double withdrawal) const;
	void showStatementOnScreen(const string&) const;
	void showMatchingTransactionsOnScreenDouble(double, int, string) const;
	void showMatchingTransactionsOnScreenString(string, int, string) const;
	void showMatchingTransactionsOnScreenDate(Date, int, string) const;

	void showAllDepositsOnScreen(bool, string, double) const;
	void showMiniStatementOnScreen(bool, double, string) const;
	void showFundsAvailableOnScreen(bool, string, double) const;
	void showTransactionsUpToDateOnScreen(bool, double, const string) const;

	void showNoTransactionsOnScreen() const;
	void showSearchMenu() const;

	void showDeletionOfTransactionsUpToDateOnScreen(const double, const Date) const;
	void showTransferOnScreen(bool trOut, bool trIn, double transferAmount, string activeAccountType, string recipientAccountType) const;
private:
	UserInterface(){};
	UserInterface(UserInterface& const) {};
	UserInterface& operator=(UserInterface& const) {};
	static UserInterface* m_pInstance;

	Time time;
	Date date;

	//support functions
	int readInCommand() const;
	double readInPositiveAmount() const;
	void outputHeader(const string&) const;
	string askForInput(const string&) const;
	void outputLine(const string&) const;
};

#endif